<div id="rev-slider">
    <div id="rev_slider_46_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric1">
        <div id="rev_slider_46_1" class="rev_slider fullscreenbanner" data-version="5.0.7">
            <ul>
                <?php foreach ($slider as $key => $row) {
                    $image_preview_url = $this->main->image_preview_url($row->thumbnail); ?>
                    <li data-index="rs-<?php echo $key ?>"
                        data-transition="zoomout"
                        data-slotamount="default"
                        data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="0">
                        <img src="<?php echo $image_preview_url ?>"
                             alt="<?php echo $row->title ?>"
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10" class="rev-slidebg">
                        <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
                             id="slide-148-layer-1"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-40','-40','-40','-40']"
                             data-fontsize="['70','70','70','45']"
                             data-lineheight="['70','70','70','50']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="200"
                             data-splitin="chars"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05">
                            <?php echo $row->title ?>
                        </div>
                        <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0 text-center"
                             id="slide-149-layer-2"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['15','15','15','15']"
                             data-fontsize="['18','16','16','14']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on">
                            <?php echo $row->description ?>
                        </div>
                        <div class="tp-caption tp-caption-mouse rev-btn rev-bordered   rs-parallaxlevel-0"
                             id="slide-214-layer-3"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']"
                             data-fontsize="['13','13','13','13']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:500;e:Linear.easeNone;"
                             data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"
                             data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="x:[100%];s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="800"
                             data-splitin="none"
                             data-splitout="none"
                             data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
                             data-responsive_offset="off"
                             data-responsive="off"
                             data-toggle="modal"
                             data-target="#modal-book">
                            <i class="fa fa-check"></i> <?php echo $dict_free_trial ?>
                        </div>
                        <div class="tp-caption rev-scroll-btn  rs-parallaxlevel-0"
                             id="slide-150-layer-4"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                             data-width="35"
                             data-height="55"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-style_hover="cursor:pointer;"
                             data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="y:50px;opacity:0;s:1000;s:1000;"
                             data-start="1200"
                             data-splitin="none"
                             data-splitout="none"
                             data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]'
                             data-basealign="slide"
                             data-responsive_offset="off"
                             data-responsive="off">
                            <span> </span>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<div class="main-area top-download text-center ">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <p class="top-btn-text">
                    <?php echo $home_sesi_1->title ?>
                </p>
            </div>
            <div class="col-md-2 btn-ma">
                <a href="#" class="theme-btn btn-style-white" data-toggle="modal" data-target="#modal-book">
                    <i class="fa fa-check"></i> <?php echo $dict_book_now ?>
                </a>
            </div>
        </div>
    </div>
</div>

<div id="superb-services">
    <div class="startup">
        <div class="container">
            <div class="section-title center text-center">
                <h2 class="wow fadeInDown"><?php echo $home_sesi_2->title ?></h2>
                <hr>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-10 col-md-offset-1 col-sm-12">
                    <?php echo $home_sesi_2->description ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="join-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="carousel-example-generic-2" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h1 align="center" class="text-center" style="text-align: center !important;">
                                        <?php echo $home_sesi_3->title ?>
                                    </h1>
                                    <?php echo $home_sesi_3->description ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="list-o list-alasan">
                                        <ul>
                                            <?php $home_sesi_3_data_1 = json_decode($home_sesi_3->data_1, TRUE);
                                            foreach ($home_sesi_3_data_1['title'] as $key => $row) { ?>
                                                <li><?php echo ($key + 1) . '. ' . $row ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 hidden-xs">
                                    <div class="img-responsive text-center align-middle alasan-robot">
                                        <img src="<?php echo base_url('assets/template_front/img/robot-trading-auto-pilot.png') ?>" alt="Robot Trading Auto Pilot">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="customizable">
    <div class="join-us-app">
        <div class="container-fluid">
            <div class="section-title center text-center">
                <h2><?php echo $faq->title ?></h2>
                <hr>
                <div class="black"><?php echo $faq->description ?></div>
            </div>
            <div class="row">
                <div class="lead-creative col-sm-6 col-xm-12 text-center ">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="<?php echo base_url() ?>assets/template_front/img/frequently-asked-question.jpg"
                                     alt="frequently Asked Question">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-xm-12 center ">
                    <div class="accordion">
                        <div class="panel-group" id="accordion1">
                            <?php $faq_data_1 = json_decode($faq->data_1, TRUE) ?>
                            <?php foreach ($faq_data_1['title'] as $key => $title) { ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading<?php echo $key == 0 ? ' active' : '' ?>">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                               data-parent="#accordion1" href="#collapseOne<?php echo $key ?>">
                                                <?php echo $title ?>
                                                <i class="fa fa-angle-right pull-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseOne<?php echo $key ?>"
                                         class="panel-collapse collapse<?php echo $key == 0 ? ' in' : '' ?>">
                                        <div class="panel-body">
                                            <div class="media accordion-inner">
                                                <div class="media-body">
                                                    <p><?php echo $faq_data_1['description'][$key] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="benefit" class="benefit ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 benefit-right no-pad">
                <div class="benefit-text no-pad">
                    <div class="benefit-heading ">
                        <div class="col-md-12 col-sm-12">
                            <h3>
                                <?php echo $home_sesi_5->title ?>
                            </h3>
                            <div style="padding-left: 20px">
                                <?php echo $home_sesi_5->description ?>
                                <?php $home_sesi_5_data_1 = json_decode($home_sesi_5->data_1, TRUE); ?>
                                <ol class="list-o">
                                    <?php foreach ($home_sesi_5_data_1['title'] as $key => $title) { ?>
                                        <li><?php echo ($key + 1) . '. ' . $title ?></li>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="superb-services">
    <div class="startup text-center">
        <div class="container">
            <div class="section-title center text-center">
                <h2 class="wow fadeInDown"><?php echo $home_sesi_6->title ?></h2>
                <hr>
                <?php echo $home_sesi_6->description ?>
            </div>
            <div class="row">
                <?php $home_sesi_6_data_1 = json_decode($home_sesi_6->data_1, TRUE); ?>
                <?php foreach ($home_sesi_6_data_1['title'] as $key => $title) {
                    $number = $this->main->translate_number($key + 1);
                    $image_preview_url = $this->main->image_preview_url($home_sesi_6_data_1['images'][$key]) ?>
                    <div class="col-md-6 col-xs-12 boxs <?php echo $number ?>">
                        <div class="single_services ">
                            <img src="<?php echo $image_preview_url ?>" alt="<?php echo $title ?>" class="alasan-img">
                        </div>
                        <h4><?php echo $title ?></h4>
                        <p><?php echo $home_sesi_6_data_1['description'][$key] ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div id="video" class="html-video-i">
    <div class="home_overlay">
        <img src="<?php echo base_url() ?>assets/template_front/img/grafik-trading-forex.jpg" alt="Grafik Trading Forex">
        <div class="video-text video-back">
            <h2 class="wow fadeInDown"><span class="top-heading"><a href="#" data-toggle="modal" data-target="#modal-book"><?php echo $dict_book_now ?></a></span></h2>
            <?php echo $home_sesi_7->description ?>
        </div>
    </div>
</div>
<?php if($show_hide) { ?>
<div id="blog" class="blog-div-area">
    <div class="container">
        <div class="row">
            <div class="section-title center text-center">
                <h2><strong><span class="yellow">Blog</span></strong> dari MAX 99</h2>
                <hr>
                <div class="black"> Berikut merupakan beberapa daftar Blog terakhir Kami.
                </div>
            </div>
        </div>
        <div class="blog-content-div">
            <div class="row">
                <?php foreach ($blog_related as $row) { ?>
                    <div class="col-md-4 col-sm-4 col-xs-12 blog-ma artikel-item">
                        <div class="single-blog">
                            <div class="feature-images">
                                <a href="<?php echo $this->main->permalink(array('blog', $row->title)) ?>">
                                    <img alt="<?php echo $row->title ?>" src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>">
                                </a>
                            </div>
                            <div class="blog-content">
                                <h3>
                                    <a href="<?php echo $this->main->permalink(array('blog', $row->title)) ?>"><?php echo $row->title ?></a>
                                </h3>
                                <p align="justify"><?php echo substr(strip_tags($row->description), 0, 150) ?> ...</p>
                                <div class="yellow-back"><a
                                            href="<?php echo $this->main->permalink(array('blog', $row->title)) ?>">Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

