<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url() ?>assets/template_front/img/favicon.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/template_front/css/max99.min.css">
    <link href=”<?php echo base_url() ?>assets/template_front/img/favicon.png" rel="apple-touch-icon" />

</head>

<body>
<nav class="navbar-fixed-top transparrent-bg">
    <div class="container">
        <div id="menuzord" class="menuzord red">
            <a href="<?php echo site_url() ?>" class="menuzord-brand">
                <img class="changeable" src="<?php echo base_url() ?>assets/template_front/img/logo-max-99-header.png"
                     alt="Logo Max 99">
            </a>
            <ul class="menuzord-menu mp_menu">
                <li<?php echo $page->type == 'home' ? ' class="active"' : '' ?>>
                    <a href="<?php echo site_url() ?>"><?php echo $home->title_menu ?></a>
                </li>
                <li<?php echo $page->type == 'about_us' ? ' class="active"' : '' ?>>
                    <a href="<?php echo site_url('tentang-kami') ?>"><?php echo $about_us->title_menu ?></a>
                </li>
                <?php if($show_hide) { ?>
                <li<?php echo $page->type == 'blog' ? ' class="active"' : '' ?>>
                    <a href="<?php echo site_url('blog') ?>"><?php echo $blog->title_menu ?></a>
                </li>
                <?php } ?>
                <li<?php echo $page->type == 'testimonial' ? ' class="active"' : '' ?>>
                    <a href="<?php echo site_url('testimonial') ?>"><?php echo $testimonial->title_menu ?></a>
                </li>
                <li<?php echo $page->type == 'faq' ? ' class="active"' : '' ?>>
                    <a href="<?php echo site_url('frequently-asked-question') ?>"><?php echo $faq->title_menu ?></a>
                </li>
                <li<?php echo $page->type == 'contact_us' ? ' class="active"' : '' ?>><a
                            href="<?php echo site_url('kontak-kami') ?>"><?php echo $contact_us->title_menu ?></a></li>
                <li class="hidden-desktop hidden-lg">
                    <a href="<?php echo site_url('free-ebook') ?>"><?php echo $free_ebook->title_menu ?></a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->main->image_preview_url($lang_active->thumbnail) ?>"
                             height="10"
                             style="height: 20px"
                             alt="<?php echo $lang_active->title ?>"
                        >
                    </a>
                    <ul class="dropdown">
                        <?php foreach ($language_list as $row) { ?>
                            <li>
                                <?php echo anchor($this->lang->switch_uri($row->code), '<img
															src="' . $this->main->image_preview_url($row->thumbnail) . '"
															height="20"
															style="height: 24px; margin-right: 10px;"
															alt="' . $row->title . '">' . $row->title) ?>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            <div class="right_mp_menu">
                <a href="<?php echo site_url('free-ebook') ?>" class="theme-btn btn-style-pinks-nav"><i
                            class="fa fa-book"></i> <?php echo $free_ebook->title_menu ?></a>
            </div>
        </div>
    </div>
</nav>
<?php echo $content ?>
<div class="main-area top-download text-left">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <?php echo $footer ?>
            </div>
            <div class="col-md-2 btn-ma">
                <a href="#" class="theme-btn btn-style-white btn-large" data-toggle="modal" data-target="#modal-book">
                    <i class="fa fa-check"></i> <?php echo $dict_book_now ?>
                </a>
            </div>
        </div>
    </div>
</div>

<form action="<?php echo site_url('free-trial') ?>" method="post" class="form-send">
    <div class="modal fade" id="modal-book" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel" align="center">
                        <?php echo $popup_trial->title ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <img src="<?php echo base_url('assets/template_front/img/robot-trading-auto-pilot.png') ?>"
                                 class="img-responsive"
                                alt="Robot Traing Auto Pilot">
                            <div class="text-center">
                                <img src="<?php echo base_url() ?>assets/template_front/img/logo-max-99-header-grey.png" alt="Logo Max 99 Header">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?php echo $popup_trial->description ?>
                            <div class="form-group">
                                <label class="required"><?php echo $dict_name ?></label>
                                <input type="text" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="required"><?php echo $dict_email ?></label>
                                <input type="text" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="required"><?php echo $dict_handphone ?></label>
                                <input type="text" name="phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="required"><?php echo $dict_message ?></label>
                                <textarea class="form-control" name="message"></textarea>
                            </div>
                            <div class="form-group">
                                <div style="width: 100%">
                                    <label class="required"><?php echo $dict_security_code ?></label>
                                </div>
                                <div class="clearfix"></div>
                                <?php echo $captcha ?>
                                <br/>
                                <br/>
                                <input type="text" name="captcha" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="theme-btn btn btn-default" data-dismiss="modal"><?php echo $dict_back ?></button>
                    <button type="submit" class="theme-btn btn btn-success"><i class="fa fa-check"></i> <?php echo $dict_message_send ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="footer-section-area padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                <div class="text-center">
                    <img src="<?php echo base_url() ?>assets/template_front/img/logo-max-99-footer.png" alt="Logo Max 99  Footer">
                </div>
                <br/>
                <ul class="address">
                    <li><a href="<?php echo $address_link ?>" target="_blank"> <i
                                    class="fa fa-map-marker fa-2x"></i> <?php echo $address ?></a></li>
                    <li><a href="<?php echo $email_link ?>"><i class="fa fa-envelope-o fa-2x"></i><?php echo $email ?>
                        </a></li>
                    <li><a href="<?php echo $phone_link ?>"><i class="fa fa-phone fa-2x"></i><?php echo $phone ?></a>
                    </li>
                </ul>
                <br/>
                <div class="social-media">
                    <h5 class="white"><?php echo $dict_social_media ?> Max99</h5>
                    <ul>
                        <li>
                            <a href="<?php echo $instagram_link ?>" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $facebook_link ?>" target="_blank">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $twitter_link ?>" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                <div class="title">
                    <h3><?php echo $dict_top_menu ?></h3>
                </div>
                <div class="information">
                    <ul class="address">
                        <li><a href="<?php echo site_url() ?>"> <i class="fa fa-arrow-right"></i> <?php echo $home->title_menu ?></a></li>
                        <li><a href="<?php echo site_url('tentang-kami') ?>"> <i class="fa fa-arrow-right"></i> <?php echo $about_us->title_menu ?></a></li>
                        <?php if($show_hide) { ?>
                        <li><a href="<?php echo site_url('blog') ?>"> <i class="fa fa-arrow-right"></i> <?php echo $blog->title_menu ?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo site_url('testimonial') ?>"> <i class="fa fa-arrow-right"></i> <?php echo $testimonial->title_menu ?></a>
                        </li>
                        <li><a href="<?php echo site_url('frequently-asked-question') ?>"> <i
                                        class="fa fa-arrow-right"></i> <?php echo $faq->title_menu ?></a></li>
                        <li><a href="<?php echo site_url('kontak-kami') ?>"> <i class="fa fa-arrow-right"></i> <?php echo $contact_us->title_menu ?></a></li>
                        <li><a href="<?php echo site_url('free-ebook') ?>"> <i class="fa fa-arrow-right"></i> <?php echo $free_ebook->title_menu ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="copy-right-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12">
                <div class="copy-right text-center">
                    <p><?php echo $dict_development_by ?> <a href="https://www.intiru.com" target="_blank">Intiru</a> &copy; 2019</p>
                </div>
            </div>
        </div>
    </div>
</div>

<span id="base-data" data-site-url="<?php echo site_url() ?>" data-base-url="<?php echo base_url() ?>"></span>
<a href="#" id="back-to-top" title="Back to top"><img
            src="<?php echo base_url() ?>assets/template_front/img/top-arrow.png" alt="Top Arrow"></a>

<div class='container-loading hide'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/max99.min.js"></script>
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5e5d99a2f2eb411bb5721db1/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>

</body>
</html>