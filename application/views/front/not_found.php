
	<div class="error-page-area">
		<div class="container">
			<div class="row">
				<!--404 text image-->
				<div class="col-xl-12">
					<div class="error-text">
					<img src="<?php echo base_url() ?>assets/front/img/404.png" alt="">
						<h2>Halaman yang Anda cari tidak ada,<br />mohon pilih menu yang ada di atas.</h2>
						<a href="<?php echo site_url() ?>">Kembali ke Beranda</a>
					</div>
				</div>
			</div>
		</div>
	</div>