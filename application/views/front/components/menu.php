<header id="home">
    <div class="main-navigation-1">
        <div class="container">
            <div class="row">
                <div class="col-xl-2 col-lg-3 col-md-3">
                    <div class="logo-area">
                        <a href="<?php echo site_url() ?>"><img src="<?php echo base_url() ?>assets/front/img/logo-sehat-kerjaku.png" alt="Logo Sehat Kerjaku"></a>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-9 col-md-9">
                    <div class="main-menu f-right">
                        <nav id="mobile-menu">
                            <ul>
                                <li>
                                    <a <?php echo $page->type == 'home' ? ' class="current"':'' ?> href="<?php echo site_url() ?>">Beranda</a>
                                </li>
                                <li>
                                    <a <?php echo $page->type == 'profile' ? ' class="current"':'' ?> href="<?php echo site_url('profil-kami') ?>">Profil Kami</a>
                                </li>
                                <li>
                                    <a <?php echo $page->type == 'blog' ? ' class="current"':'' ?> href="<?php echo site_url('artikel') ?>">Artikel</a>
                                </li>
                                <li>
                                    <a <?php echo $page->type == 'services' ? ' class="current"':'' ?> href="<?php echo site_url('layanan-kami') ?>">Layanan Kami</a>
                                </li>
                                <li>
                                    <a <?php echo $page->type == 'gallery_photo' ? ' class="current"':'' ?> href="<?php echo site_url('galeri-foto') ?>">Galeri Foto</a>
                                </li>
                                <li>
                                    <a <?php echo $page->type == 'contact_us' ? ' class="current"':'' ?> href="<?php echo site_url('kontak-kami') ?>">Kontak Kami</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="mobile-menu"></div>
                    <div class="search-box-area">
                        <div id="search" class="fade">
                            <a href="#" class="close-btn" id="close-search">
                                <em class="fa fa-times"></em>
                            </a>
                            <form action="<?php echo site_url('artikel') ?>" method="get">
                                <input name="search" placeholder="Apa yang sedang Anda cari ? Silahkan ketik disini ..." id="searchbox" type="search"  style="width: 1000px !important;"/>
                            </form>
                        </div>
                        <div class="search-icon-area">
                            <a href='#search'>
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>