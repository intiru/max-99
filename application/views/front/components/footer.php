<div class="callto-action">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-7">
                <div class="callto-action-text">
                    <h5>Tertarik dengan Pelayanan yang Kami sediakan ? <span>Mari Berdiskusi</span> </h5>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5">
                <div class="callto-action-btn">
                    <a href="<?php echo site_url('kontak-kami') ?>"><i class="fa fa-phone"></i> Kontak Kami</a>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="footer-fixed">
    <div class="big-footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sm-12">
                    <div class="footer-logo text-center">
                        <a href="<?php echo site_url() ?>" class="text-center footer-img">
                            <img src="<?php echo base_url() ?>assets/front/img/logo-sehat-kerjaku.jpeg" alt="">
                        </a>
                        <?php echo $footer ?>
                    </div>
                    <div class="social text-center">
                        <ul>
                            <li><a class="footer-socials" href="https://www.facebook.com/Sehat-Kerjaku-110404140430475/" target="_blank"><i class="fab fa-facebook"></i></a></li>
                            <li><a class="footer-socials" href="https://www.instagram.com/sehatkerjaku/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <p>Maintenanced by <a href="https://intiru.com">INTIRU.COM</a> 2019</p>
    </footer>
</section>