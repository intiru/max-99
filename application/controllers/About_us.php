<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $page = $this
            ->db
            ->where(array('id_language' => $data['id_language']))
            ->where_in('type', array('about_us','home_sesi_6'))
            ->get('pages')
            ->result();
        foreach($page as $row) {
            $data[$row->type] = $row;
        }

        $data['page'] = $data['about_us'];
        $this->template->front('tentang_kami', $data);
    }

    public function team($id)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where('id', $id)
            ->get('team')
            ->row();
        $data['page']->type = 'profile';

        $this->template->front('team_detail', $data);
    }
}
