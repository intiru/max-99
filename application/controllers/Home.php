<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();

        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['slider'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->order_by('id', 'ASC')->get('slider')->result();
        $sesi_home = $this->db
            ->where_in('type', array(
                'home_sesi_1',
                'home_sesi_2',
                'home_sesi_3',
                'faq',
                'home_sesi_5',
                'home_sesi_6',
                'home_sesi_7',
            ))
            ->where('id_language', $data['id_language'])
            ->get('pages')
            ->result();

        foreach ($sesi_home as $row) {
            $data[$row->type] = $row;
        }

//        $data['blog_related'] = $this
//            ->db
////            ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
////            ->join('team', 'team.id = blog.id_team', 'left')
////            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
//            ->where('blog.use', 'yes')
//            ->where('blog.id_language', $data['id_language'])
//            ->order_by('blog.id', 'DESC')
//            ->get('blog', 3, 0)
//            ->result();

        $this->template->front('home', $data);

    }
}
